<%@page import="model.Motorista"%>
<%@page import="model.Objeto"%>
<%@page import="model.Veiculo"%>
<%@page import="java.util.Random"%>
<%@page import="model.dao.VeiculoDao"%>
<%@page import="model.dao.MotoristaDao"%>
<%@page import="model.dao.ObjetoDao"%>
<%@page import="model.dao.DaoFactory"%>
<%@page import="resources.AppConsts"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%		
	ObjetoDao objetos = DaoFactory.getDaoFactory().getObjetoDao();
	MotoristaDao motoristas = DaoFactory.getDaoFactory().getMotoristaDao();
	VeiculoDao veiculos = DaoFactory.getDaoFactory().getVeiculoDao();
	
	Objeto objeto = null;
	Motorista motorista = null;
	Veiculo veiculo = null;
	Random randNumber = new Random();
	int indexObjeto = 0;
	int indexMotorista = 0;
	int indexVeiculo = 0;
	
	if( objetos.getObjetoList().size() > 0 ){
		indexObjeto = randNumber.nextInt(objetos.getObjetoList().size()) + 0;
		objeto = objetos.getObjetoList().get( indexObjeto );
	}
	
	if(motoristas.getMotoristaList().size() > 0 ){
		indexMotorista = randNumber.nextInt(motoristas.getMotoristaList().size()) + 0;
		motorista = motoristas.getMotoristaList().get( indexMotorista );
	}
	
	if( veiculos.getVeiculoList().size() > 0){			
		indexVeiculo = randNumber.nextInt(veiculos.getVeiculoList().size()) + 0;
		veiculo = veiculos.getVeiculoList().get( indexVeiculo );
	}
%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Home</title>
		<link href=<%=AppConsts.CAMINHO+ "/img/favicon.ico" %> rel="shortcut icon" type="image/x-icon" />
		<link href="https://fonts.googleapis.com/css?family=Bungee+Inline|Tajawal" rel="stylesheet">
		<link href=<%=AppConsts.CAMINHO+ "/css/bootstrap.css" %> rel="stylesheet">
		<link href=<%=AppConsts.CAMINHO+ "/css/ui.css" %> rel="stylesheet">
		<script type="text/javascript" src="js/solid.js" ></script>
		<script type="text/javascript" src="js/fontawesome.js" ></script>
	</head>
	<body>
		<div class="wrapper">
			<jsp:include page="/includes/sidemenu.jsp" />
			<div class="content">
				<jsp:include page="/includes/header.jsp" />
				<div id="homecont">
					<div id="home" class="cadcont">
						<h2 class="ola"> Ol�, <%= " "+session.getAttribute( "usuario" ) %>. </h2>
						
						
						<p>
							A Transportadora Entrega R�pida � uma empresa dedicada ao transporte de carga por todo o Brasil. 
							Oferecemos tr�s tipos de ve�culos que se adaptam a necessidade do cliente, tendo motoristas qualificados e testados periodicamente.
						</p>
						
						<p> <b>Curiosidade:</b>
							Em lingu�stica, a no��o de texto � ampla e ainda aberta a uma
							defini��o mais precisa. Grosso modo, pode ser entendido como
							manifesta��o lingu�stica das ideias de um autor, que ser�o
							interpretadas pelo leitor de acordo com seus conhecimentos
							lingu�sticos e culturais. Seu tamanho � vari�vel.
						</p>
						

					</div>
					<%if (objeto != null || motorista != null || veiculo != null){%>				
					<div class="cardCont">
					<br/>
					
					<h3 class="ola" style="margin-left: 10px;">Destaques</h3>
					
					<% if (objeto != null){%>
						<div class="card">
						<h5 style="text-align:center;">Objeto</h5>
							<img class="card-img-top" src=<%=AppConsts.CAMINHO+ "/img/objeto.jpg" %> alt="Card image cap">
							<div class="card-body">
								<h5 class="card-title ola">N� <%= objeto.getCodigoLocalizador(  )%></h5>
								<p class="card-text">O objeto foi cadastrado para a entrega e a sua encomenda est� sendo preparada! </p>
								<a href=<%=AppConsts.CAMINHO + "/ui.cadastro/relatorio/relatorioObjeto.jsp" %> class="btn btn-primary">Mais...</a>
							</div>
						</div>
						
					<%} if (motorista != null){%>
						<div class="card">
						<h5 style="text-align:center;">Motorista</h5>
							<img class="card-img-top" src=<%=AppConsts.CAMINHO+ "/img/motorista.jpg" %> alt="Card image cap">
							<div class="card-body">
								<h5 class="card-title ola"><%= motorista.getNome(  ) %></h5>
								<p class="card-text">Todos os nossos motoristas so treinados e testados periodicamente para melhor atende-lo! Agilidade e seguran�a s�o a nossa miss�o. </p>
								<a href=<%=AppConsts.CAMINHO + "/ui.cadastro/relatorio/relatorioMotorista.jsp" %> class="btn btn-primary">Mais...</a>
							</div>
						</div>
						                                                                         
					<%} if (veiculo != null){%>
						<div class="card">
							<h5 style="text-align:center;">Ve�culo</h5>
							<img class="card-img-top" src=<%=AppConsts.CAMINHO+ "/img/veiculo.jpg" %> alt="Card image cap">
							<div class="card-body">
								<h5 class="card-title ola"><%= veiculo.getModelo(  ).getNome(  ) %></h5>
								<p class="card-text">A nossa frota de transfer�ncia e de distribui��o � equipada com sistemas de rastreamento via sat�lite e r�dio. 
								Os ve�culos s�o equipados com on-board computer e dispositivos de seguran�a que permitem o travamento das portas, o corte de combust�vel, 
								o acionamento do alarme e o bloqueio do desengate da carreta. Tudo isso supervisionado 24 horas por dia, por uma equipe especializada no gerenciamento de risco de transporte. </p>
								<a href=<%=AppConsts.CAMINHO + "/ui.cadastro/relatorio/relatorioVeiculo.jsp" %> class="btn btn-primary">Mais...</a>
							</div>
						</div>
						<%} %>
					</div>
					<%} %>
				</div>
			</div>
			<div class="footer">
				<jsp:include page="/includes/footer.html" />
			</div>
		</div>
		<script type="text/javascript" src="js/jquery-3.3.1.js"></script>   
		<script type="text/javascript" src="js/popper-1-14-3.js"></script>
		<script type="text/javascript" src="js/bootstrap.js"></script>
		<script type="text/javascript" src="js/scrollercdn.js"></script>
		<script type="text/javascript" src="js/ui.js"></script>
		<script type="text/javascript">
		
		
		
			// 			$(function(){
			<%-- 				<% if(session.getAttribute( "usuario" ) == null)response.sendRedirect( AppConsts.CAMINHO+"/index.jsp" );%> --%>
			// 			})			
			// 			$("#btnsair").click(function(){
			<%-- 				<% session.setAttribute( "usuario", null ); %> --%>
			// 			});
					
		</script>
	</body>
</html>
