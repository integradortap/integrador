<%@page import="java.util.List"%>
<%@page import="resources.GerenciadorData"%>
<%@page import="model.dao.MotoristaDao"%>
<%@page import="model.dao.DaoFactory"%>
<%@page import="model.Motorista"%>
<%@page import="resources.AppConsts"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%
	MotoristaDao motoristas = DaoFactory.getDaoFactory().getMotoristaDao();
	String tipoCNH = request.getParameter( "tipoCNH" ) == null ? "" : request.getParameter( "tipoCNH" );
	List<Motorista> motoristaList = motoristas.getMotoristaListByTipoCNH(tipoCNH);
%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Relat�rio de Motoristas</title>
		<link href=<%=AppConsts.CAMINHO+ "/img/favicon.ico" %> rel="shortcut icon" type="image/x-icon" />
		<link href="https://fonts.googleapis.com/css?family=Bungee+Inline|Tajawal" rel="stylesheet">
		<link href=<%=AppConsts.CAMINHO+ "/css/bootstrap.css" %> rel="stylesheet">
		<link href=<%=AppConsts.CAMINHO+ "/css/ui.css" %> rel="stylesheet">
		<script type="text/javascript" src="../../js/solid.js" ></script>
		<script type="text/javascript" src="../../js/fontawesome.js" ></script>
	</head>
	<body>
		<div class="wrapper">
			<jsp:include page="../../includes/sidemenu.jsp" />
			<div class="content">
				<jsp:include page="../../includes/header.jsp" />
				
				<div id="homecont">				
					<div id="home" class="cadcont">
						<h3>Relat�rio de Motoristas</h3>
						
						<div class="form-group row">
							<div class="col-sm-5">
							<form method="post" action=<%=AppConsts.CAMINHO + "/BuscaRelatorioServlet"%>>
								<div class="input-group" >
									<div class="input-group-prepend">
										<label class="input-group-text">Busca por tipo CNH</label>
									</div>
									<select class="form-control sm" name="filtro" >
										<option value="">Todos</option>
										<option value="B">B</option>
										<option value="C">C</option>
									</select>
									<input type="hidden" name="tipoBusca" value="motorista"/>
									<div class="input-group-append" >
										<button type="submit" class="btn btn-primary" >Buscar</button>
									</div>
								</div>
							</form>						
							</div>
						</div>
						<br/>
						
						<%
							out.println("<table style=width:100% >");
							out.println("<tr id=trlabels >");						
							out.println("<td>Nome</td>");
							out.println("<td>Data de Nascimento</td>");
							out.println("<td>CNH</td>");
							out.println("<td>Tipo CNH</td>");							
							out.println("<td>Endere�o</td>");
							out.println("<td>Disponibilidade</td>");
							out.println("</tr>");

							int i = 0;
							for(Motorista motorista : motoristaList){
								if(motorista != null){
									out.println("<tr>");							
									out.println("<td>");						
									out.println("<input class=form-control name=nome value='"+motorista.getNome()+"' disabled />");
									out.println("</td>");						
									out.println("<td>");						
									out.println("<input class=form-control name=data value='"+GerenciadorData.getInstance().dateToStr(motorista.getDataNasc())+"' disabled />");
									out.println("</td>");						
									out.println("<td>");						
									out.println("<input class=form-control name=cnh value='"+motorista.getCNH()+"' disabled />");
									out.println("</td>");						
									out.println("<td>");						
									out.println("<input class=form-control name=tipoCNH value='"+ motorista.getTipoCNH() +"' disabled />");
									out.println("</td>");						
									out.println("<td>");						
									out.println("<input class=form-control name=endereco value='"+motorista.getEndereco()+"' disabled />");
									out.println("</td>");						
									out.println("<td>");						
									out.println("<input class=form-control name=disponibilidade value='"+motorista.getDisponibilidade()+"' disabled />");
									out.println("</td>");						
									out.println("<tr>");
		
									i++;
								}
							}
							out.println("</table>");
						%>	
					</div>
				</div>
				
			</div>
			<div class="footer">
				<jsp:include page="../../includes/footer.html" />
			</div>
		</div>
		<script type="text/javascript" src="../../js/jquery-3.3.1.js"></script>   
		<script type="text/javascript" src="../../js/popper-1-14-3.js"></script>
		<script type="text/javascript" src="../../js/bootstrap.js"></script>
		<script type="text/javascript" src="../../js/scrollercdn.js"></script>
		<script type="text/javascript" src="../../js/ui.js"></script>
	</body>
</html>