<%@page import="java.util.List"%>
<%@page import="resources.GerenciadorData"%>
<%@page import="model.dao.ObjetoDao"%>
<%@page import="model.Objeto"%>
<%@page import="model.dao.MotoristaDao"%>
<%@page import="model.dao.DaoFactory"%>
<%@page import="model.Motorista"%>
<%@page import="resources.AppConsts"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%

	ObjetoDao objetos = DaoFactory.getDaoFactory().getObjetoDao();
	String status = request.getParameter( "status" ) == null ? "" : request.getParameter( "status" );
	List<Objeto> objetoList = objetos.getObjetoListByStatus(status);
%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Relat�rio de Motoristas</title>
		<link href=<%=AppConsts.CAMINHO+ "/img/favicon.ico" %> rel="shortcut icon" type="image/x-icon" />
		<link href="https://fonts.googleapis.com/css?family=Bungee+Inline|Tajawal" rel="stylesheet">
		<link href=<%=AppConsts.CAMINHO+ "/css/bootstrap.css" %> rel="stylesheet">
		<link href=<%=AppConsts.CAMINHO+ "/css/ui.css" %> rel="stylesheet">
		<script type="text/javascript" src="../../js/solid.js" ></script>
		<script type="text/javascript" src="../../js/fontawesome.js" ></script>
	</head>
	<body>
		<div class="wrapper">
			<jsp:include page="../../includes/sidemenu.jsp" />
			<div class="content">
				<jsp:include page="../../includes/header.jsp" />
				
				<div id="homecont">				
					<div id="home" class="cadcont">
						<h3>Relat�rio de Objetos</h3>
												
						<div class="form-group row">
							<div class="col-sm-5">
							<form method="post" action=<%=AppConsts.CAMINHO + "/BuscaRelatorioServlet"%>>
								<div class="input-group" >
									<div class="input-group-prepend">
										<label class="input-group-text">Busca por Status</label>
									</div>
									<select class="form-control sm" name="filtro" >
										<option value="">Todos</option>
										<option value="Postado">Postado</option>
										<option value="Pendente">Pendente</option>
										<option value="Em Entrega">Em Entrega</option>
										<option value="Entregue">Entregue</option>
									</select>
									<input type="hidden" name="tipoBusca" value="objeto"/>
									<div class="input-group-append" >
										<button type="submit" class="btn btn-primary" >Buscar</button>
									</div>
								</div>
							</form>						
							</div>
						</div>
						<br/>
						<%
							out.println("<table style=width:100% >");
							out.println("<tr id=trlabels >");						
							out.println("<td>C�digo Localizador</td>");
							out.println("<td>Peso</td>");							
							out.println("<td>Remetente</td>");
							out.println("<td>End. Remetente</td>");
							out.println("<td>Destinat�rio</td>");
							out.println("<td>End. Destinat�rio</td>");
							out.println("<td>Data de Dep�sito</td>");
							out.println("<td>Status</td>");
							out.println("</tr>");

							int i = 0;
							for(Objeto objeto : objetoList){
								if(objeto != null){
									out.println("<tr>");							
									out.println("<td>");						
									out.println("<input class=form-control name=codigoLocalizador value='"+objeto.getCodigoLocalizador()+"' disabled />");
									out.println("</td>");						
									out.println("<td>");						
									out.println("<input class=form-control name=peso value='"+objeto.getPeso()+"' disabled />");
									out.println("</td>");						
									out.println("<td>");						
									out.println("<input class=form-control name=nomeRemetente value='"+objeto.getNomeRemetente()+"' disabled />");
									out.println("</td>");						
									out.println("<td>");						
									out.println("<input class=form-control name=enderecoRemetente value='"+ objeto.getEnderecoRemetente() +"' disabled />");
									out.println("</td>");						
									out.println("<td>");						
									out.println("<input class=form-control name=nomeDestinatario value='"+ objeto.getNomeDestinatario() +"' disabled />");
									out.println("</td>");						
									out.println("<td>");						
									out.println("<input class=form-control name=enderecoDestinatario value='"+objeto.getEnderecoDestinatario()+"' disabled />");
									out.println("</td>");						
									out.println("<td>");						
									out.println("<input class=form-control name=data value='"+GerenciadorData.getInstance().dateToStr(objeto.getDataDeposito())+"' disabled />");
									out.println("</td>");						
									out.println("<td>");						
									out.println("<input class=form-control name=status value='"+objeto.getStatusDesc()+"' disabled />");
									out.println("</td>");						
									out.println("<tr>");
		
									i++;
								}
							}
							out.println("</table>");
						%>	
					</div>
				</div>
				
			</div>
			<div class="footer">
				<jsp:include page="../../includes/footer.html" />
			</div>
		</div>
		<script type="text/javascript" src="../../js/jquery-3.3.1.js"></script>   
		<script type="text/javascript" src="../../js/popper-1-14-3.js"></script>
		<script type="text/javascript" src="../../js/bootstrap.js"></script>
		<script type="text/javascript" src="../../js/scrollercdn.js"></script>
		<script type="text/javascript" src="../../js/ui.js"></script>
	</body>
</html>
