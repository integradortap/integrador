<%@page import="java.util.List"%>
<%@page import="model.dao.DaoFactory"%>
<%@page import="model.dao.VeiculoDao"%>
<%@page import="model.Veiculo"%>
<%@page import="resources.AppConsts"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%
	VeiculoDao veiculos = DaoFactory.getDaoFactory().getVeiculoDao();
	String modelo = request.getParameter( "modelo" ) == null ? "" : request.getParameter( "modelo" );
	List<Veiculo> veiculoList = veiculos.getVeiculoListByModelo(modelo);
%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Relat�rio de Motoristas</title>
		<link href=<%=AppConsts.CAMINHO+ "/img/favicon.ico" %> rel="shortcut icon" type="image/x-icon" />
		<link href="https://fonts.googleapis.com/css?family=Bungee+Inline|Tajawal" rel="stylesheet">
		<link href=<%=AppConsts.CAMINHO+ "/css/bootstrap.css" %> rel="stylesheet">
		<link href=<%=AppConsts.CAMINHO+ "/css/ui.css" %> rel="stylesheet">
		<script type="text/javascript" src="../../js/solid.js" ></script>
		<script type="text/javascript" src="../../js/fontawesome.js" ></script>
	</head>
	<body>
		<div class="wrapper">
			<jsp:include page="../../includes/sidemenu.jsp" />
			<div class="content">
				<jsp:include page="../../includes/header.jsp" />
				
				<div id="homecont">				
					<div id="home" class="cadcont">
						<h3>Relat�rio de Ve�culos</h3>

						<div class="form-group row">
							<div class="col-sm-5">
							<form method="post" action=<%=AppConsts.CAMINHO + "/BuscaRelatorioServlet"%>>
								<div class="input-group" >
									<div class="input-group-prepend">
										<label class="input-group-text">Busca por Modelo</label>
									</div>
									<select class="form-control sm" name="filtro" >
										<option value="">Todos</option>
										<option value="Carreta">Carreta</option>
										<option value="Caminh�o B�u">Caminh�o B�u</option>
										<option value="Van">Van</option>
									</select>
									<input type="hidden" value="veiculo" name="tipoBusca" />
									<div class="input-group-append" >
										<button type="submit" class="btn btn-primary" >Buscar</button>
									</div>
								</div>
							</form>						
							</div>
						</div>
						<br/>
						<%
							out.println("<table style=width:100% >");
							out.println("<tr id=trlabels >");						
							out.println("<td>Modelo</td>");
							out.println("<td>Marca</td>");							
							out.println("<td>Ano</td>");
							out.println("<td>Placa</td>");
							out.println("<td>Capacidade</td>");
							out.println("<td>Motorista</td>");
							out.println("<td>CNH</td>");
							out.println("<td>Disponibilidade</td>");
							out.println("</tr>");
							String testeNullNome = "N�o Cadastrado", testeNullCNH = "N�o Cadastrado";
							int i = 0;
							for(Veiculo veiculo : veiculoList){
								if(veiculo != null){
									if(veiculo.getMotorista() != null){
										testeNullNome = veiculo.getMotorista().getNome();
										testeNullCNH = veiculo.getMotorista().getCNH() + " tipo "+veiculo.getMotorista().getTipoCNH();
									}
									out.println("<tr>");							
									out.println("<td>");						
									out.println("<input class=form-control name=modelo value='"+veiculo.getModelo().getNome()+"' disabled />");
									out.println("</td>");						
									out.println("<td>");						
									out.println("<input class=form-control name=marca value='"+ veiculo.getMarca() +"' disabled />");
									out.println("</td>");						
									out.println("<td>");						
									out.println("<input class=form-control name=ano value='"+veiculo.getAno()+"' disabled />");
									out.println("</td>");						
									out.println("<td>");						
									out.println("<input class=form-control name=placa value='"+veiculo.getPlaca()+"' disabled />");
									out.println("</td>");						
									out.println("<td>");						
									out.println("<input class=form-control name=capacidade value='"+ veiculo.getModelo().getCapacidade() +"' disabled />");
									out.println("</td>");						
									out.println("<td>");						
									out.println("<input class=form-control name=nomeMotorista value='"+testeNullNome+"' disabled />");
									out.println("</td>");						
									out.println("<td>");						
									out.println("<input class=form-control name=cnh value='"+testeNullCNH+"' disabled />");
									out.println("</td>");						
									out.println("<td>");						
									out.println("<input class=form-control name=disponibilidade value='"+veiculo.getDisponibilidade()+"' disabled />");
									out.println("</td>");						
									out.println("<tr>");
		
									i++;
								}
							}
							out.println("</table>");
						%>	
					</div>
				</div>
				
			</div>
			<div class="footer">
				<jsp:include page="../../includes/footer.html" />
			</div>
		</div>
		<script type="text/javascript" src="../../js/jquery-3.3.1.js"></script>   
		<script type="text/javascript" src="../../js/popper-1-14-3.js"></script>
		<script type="text/javascript" src="../../js/bootstrap.js"></script>
		<script type="text/javascript" src="../../js/scrollercdn.js"></script>
		<script type="text/javascript" src="../../js/ui.js"></script>
	</body>
</html>
