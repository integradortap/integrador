package model;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import model.Objeto.Status;
import model.dao.DaoFactory;
import model.dao.ObjetoDao;

public class Roteiro {

	private String id;

	private Veiculo veiculo;

	private Date data;

	private List<Objeto> objetosRoteiro;

	int capacidade;

	int carga = 0;

	private boolean finalizado;

	public Roteiro(Date data, Veiculo veiculo) {
		this.data = data;
		this.veiculo = veiculo;
		this.capacidade = veiculo.getModelo().getCapacidade();
		this.objetosRoteiro = new ArrayList<>(this.capacidade);
		this.gerarRoteiro();
	}

	public void adicionaPendente() throws ParseException {
		ObjetoDao objetoDao = DaoFactory.getDaoFactory().getObjetoDao();
		for (int i = 0; this.capacidade > this.carga && i < objetoDao.getObjetoList().size(); i++) {
			Objeto objetoPendente = objetoDao.getObjetoList().get(i);

			if (objetoPendente.getStatus().equals(Status.PENDENTE)) {
				objetoPendente.setStatus(Status.EM_ENTREGA);
				objetoDao.setStatus(objetoPendente.getId(), Status.EM_ENTREGA);
				this.objetosRoteiro.add(objetoPendente);
				this.carga++;

			}
		}
	}

	public void adicionaPostado() throws ParseException {
		ObjetoDao objetoDao = DaoFactory.getDaoFactory().getObjetoDao();
		for (int i = 0; this.capacidade > this.carga && i < objetoDao.getObjetoList().size(); i++) {
			Objeto objetoPostado = objetoDao.getObjetoList().get(i);

			if (objetoPostado.getStatus().equals(Status.POSTADO)) {
				objetoPostado.setStatus(Status.EM_ENTREGA);
				objetoDao.setStatus(objetoPostado.getId(), Status.EM_ENTREGA);
				this.objetosRoteiro.add(objetoPostado);
				this.carga++;
			}
		}
	}

	private void gerarRoteiro() {
		try {
			this.adicionaPendente();
			this.adicionaPostado();
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}

	public Date getData() {
		return this.data;
	}

	public String getId() {
		return this.id;
	}

	public List<Objeto> getObjetosRoteiro() {
		return this.objetosRoteiro;
	}

	public Veiculo getVeiculo() {
		return this.veiculo;
	}

	public boolean isFinalizado() {
		return this.finalizado;
	}

	public String getFinalizado() {
		return this.finalizado ? "Finalizado" : "Pendente";
	}

	public void setData(Date data) {
		this.data = data;
	}

	public void setFinalizado(boolean finalizado) {
		this.finalizado = finalizado;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setObjetosRoteiro(List<Objeto> objetosRoteiro) {
		this.objetosRoteiro = objetosRoteiro;
	}

	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}

	@Override
	public String toString() {
		return "Ve�culo: " + this.veiculo.toString();
	}
}
