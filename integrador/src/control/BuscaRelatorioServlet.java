package control;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import resources.AppConsts;

/**
 * Servlet implementation class BuscaServlet
 */
@WebServlet("/BuscaRelatorioServlet")
public class BuscaRelatorioServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public BuscaRelatorioServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		String tipoBusca = request.getParameter("tipoBusca");
		String valorSelecionado = request.getParameter("filtro");

		if (null != valorSelecionado && (null != tipoBusca && !tipoBusca.isEmpty())) {

			switch (tipoBusca) {

			case "objeto":
				response.sendRedirect(AppConsts.CAMINHO + "/ui.cadastro/relatorio/relatorioObjeto.jsp?status=" + valorSelecionado);
				break;

			case "motorista":
				response.sendRedirect(AppConsts.CAMINHO + "/ui.cadastro/relatorio/relatorioMotorista.jsp?tipoCNH=" + valorSelecionado);
				break;

			case "veiculo":
				response.sendRedirect(AppConsts.CAMINHO + "/ui.cadastro/relatorio/relatorioVeiculo.jsp?modelo=" + valorSelecionado);
				break;

			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		this.doGet(request, response);
	}

}
